package sst.com.sstopenhouse2018;

public class Booth {
    private String name;
    private String location;
    private boolean isCategory;

    public Booth(String name, String location) {
        this.name = name;
        this.location = location;
        this.isCategory = false;
    }

    public Booth(String name) {
        this.name = name;
        this.isCategory = true;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public boolean isCategory() {
        return isCategory;
    }
}
