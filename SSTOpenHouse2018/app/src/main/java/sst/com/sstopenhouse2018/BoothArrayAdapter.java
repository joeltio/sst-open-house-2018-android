package sst.com.sstopenhouse2018;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class BoothArrayAdapter extends ArrayAdapter<Booth> {
    public BoothArrayAdapter(Activity context, List<Booth> list) {
        super(context, R.layout.layout_booth_item, list);
    }

    private static class ViewHolder {
        TextView nameTextView;
        TextView descTextView;
    }

    private static class HeaderViewHolder {
        TextView headerTextView;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Booth value = getItem(position);

        if (value.isCategory()) {
            HeaderViewHolder headerViewHolder;
            if (convertView == null || (convertView.getTag() instanceof ViewHolder)) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_booth_header,
                        parent, false);
                headerViewHolder = new HeaderViewHolder();
                headerViewHolder.headerTextView = convertView.findViewById(R.id.textview_booth_item_header);

                convertView.setTag(headerViewHolder);
            } else {
                headerViewHolder = (HeaderViewHolder) convertView.getTag();
            }

            headerViewHolder.headerTextView.setText(value.getName());
        } else {
            ViewHolder viewHolder;
            if (convertView == null || (convertView.getTag() instanceof HeaderViewHolder)) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_booth_item,
                        parent, false);
                viewHolder = new ViewHolder();
                viewHolder.nameTextView = convertView.findViewById(R.id.textview_booth_item_name);
                viewHolder.descTextView = convertView.findViewById(R.id.textview_booth_item_desc);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.nameTextView.setText(value.getName());
            viewHolder.descTextView.setText(value.getLocation());
        }

        return convertView;
    }
}
