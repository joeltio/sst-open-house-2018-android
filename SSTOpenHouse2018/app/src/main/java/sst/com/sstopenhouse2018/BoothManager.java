package sst.com.sstopenhouse2018;

import android.content.res.Resources;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class BoothManager {
    public static ArrayList<Booth> getBooths(Resources resources) {
        JSONArray boothsAllJSON;
        try {
            InputStream is = resources.openRawResource(R.raw.booths);
            JSONParser jsonParser = new JSONParser();
            boothsAllJSON = (JSONArray) jsonParser.parse(new InputStreamReader(
                    is, "UTF-8"));
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }

        // Convert JSON to booth objects
        ArrayList<Booth> booths = new ArrayList<>();
        for (Object categoryObject : boothsAllJSON) {
            JSONObject categoryJSON = (JSONObject) categoryObject;
            String categoryName = (String) categoryJSON.get("category");
            JSONArray boothsJSON = (JSONArray) categoryJSON.get("booths");

            booths.add(new Booth(categoryName));
            for (Object boothObject : boothsJSON) {
                JSONObject boothJSON = (JSONObject) boothObject;
                String name = (String) boothJSON.get("name");
                String location = (String) boothJSON.get("location");
                booths.add(new Booth(name, location));
            }
        }

        return booths;
    }
}
