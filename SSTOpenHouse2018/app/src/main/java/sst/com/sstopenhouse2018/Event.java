package sst.com.sstopenhouse2018;

import java.util.Calendar;

public class Event {
    private String name;
    private String location;
    private Calendar startTime;
    private Calendar endTime;
    private String startTimeString;
    private String endTimeString;

    public Event(String name, String location) {
        this.name = name;
        this.location = location;
        this.startTimeString = "";
        this.endTimeString = "";
    }

    public Event(String name, String location, String startTimeString, String endTimeString) {
        this.name = name;
        this.location = location;

        this.startTimeString = startTimeString;
        this.endTimeString = endTimeString;

        this.startTime = dateStringToCalendar(startTimeString);
        this.endTime = dateStringToCalendar(endTimeString);
    }

    private Calendar dateStringToCalendar(String timeString) {
        int hours = Integer.parseInt(timeString.substring(0, 2));
        int minutes = Integer.parseInt(timeString.substring(2));

        Calendar date = Calendar.getInstance();
        date.set(2018, 4, 26, hours, minutes);

        return date;
    }

    public String getName() {
        return this.name;
    }

    public String getLocation() {
        return this.location;
    }

    public Calendar getStartTime() {
        return this.startTime;
    }

    public Calendar getEndTime() {
        return this.endTime;
    }

    public String getTimeSpan() {
        return this.startTimeString + " - " + this.endTimeString;
    }

    public String getStartTimeString() {
        return startTimeString;
    }

    public String getEndTimeString() {
        return endTimeString;
    }
}
