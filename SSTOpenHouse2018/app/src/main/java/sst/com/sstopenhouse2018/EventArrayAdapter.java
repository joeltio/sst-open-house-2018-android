package sst.com.sstopenhouse2018;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class EventArrayAdapter extends ArrayAdapter<Event> {
    public EventArrayAdapter(Activity context, List<Event> list) {
        super(context, R.layout.layout_event_item, list);
    }

    private static class ViewHolder {
        TextView nameTextView;
        TextView descTextView;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Event value = getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_event_item,
                    parent, false);

            viewHolder = new ViewHolder();
            viewHolder.nameTextView = convertView.findViewById(R.id.textview_event_item_name);
            viewHolder.descTextView = convertView.findViewById(R.id.textview_event_item_desc);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.nameTextView.setText(value.getName());
        String description = value.getTimeSpan() + " (" + value.getLocation() + ")";
        viewHolder.descTextView.setText(description);

        return convertView;
    }
}
