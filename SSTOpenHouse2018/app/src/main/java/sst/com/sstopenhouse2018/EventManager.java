package sst.com.sstopenhouse2018;

import android.content.res.Resources;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class EventManager {
    private static ArrayList<Event> convertToEvents(
            JSONArray eventsJSON, boolean onlyImportant, boolean showPastEvents) {
        ArrayList<Event> events = new ArrayList<>();
        Date now = new Date();

        for (Object eventObj : eventsJSON) {
            JSONObject eventJSON = (JSONObject) eventObj;
            String name = (String) eventJSON.get("name");
            String location = (String) eventJSON.get("location");

            if (onlyImportant && (!(boolean) eventJSON.get("important"))) {
                continue;
            }

            for (Object timePairObj : (JSONArray) eventJSON.get("times")) {
                JSONArray timePair = (JSONArray) timePairObj;

                String startTime = (String) timePair.get(0);
                String endTime = (String) timePair.get(1);
                Event event = new Event(name, location, startTime, endTime);

                // Filter events before now
                if (!showPastEvents) {
                    if (event.getStartTime().getTime().before(now)) {
                        continue;
                    }
                }

                events.add(event);
            }
        }

        sortEvents(events);
        return events;
    }

    private static void sortEvents(ArrayList<Event> events) {
        Collections.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event event, Event t1) {
                return event.getStartTimeString().compareTo(t1.getStartTimeString());
            }
        });
    }

    public static ArrayList<Event> getEvents(Resources resources, boolean showPastEvents) {
        JSONArray eventsJSON;
        try {
            InputStream is = resources.openRawResource(R.raw.activities);
            JSONParser jsonParser = new JSONParser();
            eventsJSON = (JSONArray) jsonParser.parse(new InputStreamReader(
                    is, "UTF-8"));
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }

        ArrayList<Event> events = convertToEvents(eventsJSON, false, showPastEvents);
        sortEvents(events);

        return events;
    }

    public static ArrayList<Event> getImportantEvents(Resources resources, boolean showPastEvents) {
        JSONArray eventsJSON;
        try {
            InputStream is = resources.openRawResource(R.raw.activities);
            JSONParser jsonParser = new JSONParser();
            eventsJSON = (JSONArray) jsonParser.parse(new InputStreamReader(
                    is, "UTF-8"));
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }

        ArrayList<Event> events = convertToEvents(eventsJSON, true, showPastEvents);
        sortEvents(events);
        return events;
    }
}
