package sst.com.sstopenhouse2018;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import sst.com.sstopenhouse2018.views.DynamicVideoView;
import sst.com.sstopenhouse2018.views.ImageCardView;

public class MainActivity extends AppCompatActivity {
    private boolean isNetworkAvailable() {
        // Note that this only check if the device is connected to a network and not to the internet
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private View.OnClickListener createLinkClickListener(final String url) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Show notice when there is no network connection
        if (!isNetworkAvailable()) {
            LinearLayout networkDownLayout = findViewById(R.id.layout_main_network_down);
            networkDownLayout.setVisibility(View.VISIBLE);
        }

        // Set the videoview's dimensions to the video's dimensions
        DynamicVideoView aboutVideo = findViewById(R.id.dynamicvideoview_main_about);
        aboutVideo.setVideoResource(R.raw.video_main_about, getPackageName(), getApplication());
        // Loop the videoview
        aboutVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setLooping(true);
                mediaPlayer.start();
            }
        });

        // Set the search image button function
        ImageButton searchButton = findViewById(R.id.imagebutton_main_search);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText searchEditText = findViewById(R.id.edittext_main_search);
                String query = searchEditText.getText().toString();

                String url = getResources().getString(R.string.main_sst_url_search) + query;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        // Link the cards to their activities
        CardView aboutView = findViewById(R.id.cardview_main_about);
        aboutView.setOnClickListener(createLinkClickListener(
                getResources().getString(R.string.main_sst_url_about)));

        CardView comingUpNextView = findViewById(R.id.cardview_main_next);
        comingUpNextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ComingUpNextActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });

        CardView activitiesView = findViewById(R.id.cardview_main_activities);
        activitiesView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ActivitiesActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });

        CardView boothsView = findViewById(R.id.cardview_main_booths);
        boothsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, BoothsActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });

        CardView mapView = findViewById(R.id.cardview_main_map);
        mapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MapActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });

        ImageCardView redemptionView = findViewById(R.id.cardview_main_redemption);
        redemptionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RedemptionActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });

        // Feedback form link
        TextView feedbackTextView = findViewById(R.id.textview_main_feedback);
        feedbackTextView.setOnClickListener(createLinkClickListener(
                getResources().getString(R.string.main_feedback_url)));

        // Social media links
        ImageButton facebookButton = findViewById(R.id.imagebutton_main_social_facebook);
        ImageButton twitterButton = findViewById(R.id.imagebutton_main_social_twitter);
        ImageButton instagramButton = findViewById(R.id.imagebutton_main_social_instagram);
        facebookButton.setOnClickListener(createLinkClickListener(
                getResources().getString(R.string.main_social_facebook_url)));
        twitterButton.setOnClickListener(createLinkClickListener(
                getResources().getString(R.string.main_social_twitter_url)));
        instagramButton.setOnClickListener(createLinkClickListener(
                getResources().getString(R.string.main_social_instagram_url)));
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Show notice when there is no network connection
        LinearLayout networkDownLayout = findViewById(R.id.layout_main_network_down);
        if (isNetworkAvailable()) {
            networkDownLayout.setVisibility(View.GONE);
        } else {
            networkDownLayout.setVisibility(View.VISIBLE);
        }
    }
}
