package sst.com.sstopenhouse2018;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import sst.com.sstopenhouse2018.redemption.CodeVerifier;
import sst.com.sstopenhouse2018.utils.DialogUtil;

public class RedemptionActivity extends AppCompatActivity {
    private final int[] cylinderResources = {
            R.drawable.machineguns_0, R.drawable.machineguns_1, R.drawable.machineguns_2,
            R.drawable.machineguns_3, R.drawable.machineguns_4, R.drawable.machineguns_5,
            R.drawable.machineguns_6, R.drawable.machineguns_7, R.drawable.machineguns_8,
            R.drawable.machineguns_9, R.drawable.machineguns_10
    };

    private static final String REDEMPTION_CODE_PREFERENCES = "RedemptionActivity.redeemed_codes";
    private static final String REDEMPTION_PREFERENCES = "RedemptionActivity";
    private CodeVerifier codeVerifier;

    private void refreshCylinder() {
        ImageView cylinderImage = findViewById(R.id.imageview_redemption_cylinder);
        int numRedeemed = this.codeVerifier.getNumCodesRedeemed();
        if (numRedeemed > 10) {
            numRedeemed = 10;
        }
        cylinderImage.setImageResource(cylinderResources[numRedeemed]);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redemption);

        Toolbar toolbar = findViewById(R.id.redemption_toolbar);
        toolbar.setTitle(R.string.redemption_title);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Button redeemButton = findViewById(R.id.button_redemption_redeem);
        Button prizesButton = findViewById(R.id.button_redemption_prizes);

        this.codeVerifier = new CodeVerifier(
                getSharedPreferences(REDEMPTION_CODE_PREFERENCES, Context.MODE_PRIVATE),
                getSharedPreferences(REDEMPTION_PREFERENCES, Context.MODE_PRIVATE));

        // Set the cylinder long press
        ImageView cylinderImage = findViewById(R.id.imageview_redemption_cylinder);
        cylinderImage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showRedeemedDialog();
                return false;
            }
        });

        // Set the refresh cylinder image
        refreshCylinder();

        // Set the button functions
        redeemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRedeemDialog();
            }
        });
        prizesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPrizesDialog();
            }
        });
    }

    private void showPrizesDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.redemption_prizes_dialog_title);

        // Set the custom view
        @SuppressLint("InflateParams")
        View prizeDialogView = this.getLayoutInflater().inflate(R.layout.layout_dialog_prizes, null);
        builder.setView(prizeDialogView);

        // Set the button functions
        builder.setNeutralButton(R.string.redemption_prizes_dialog_close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showRedeemDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.redemption_redeem_dialog_title).setMessage(R.string.redemption_redeem_dialog_desc);

        // Set the custom view to a numerical input
        final EditText input = DialogUtil.createCodeInput(this, 4);
        builder.setView(input);

        // Set the button functions
        builder.setPositiveButton(R.string.redemption_redeem_dialog_redeem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                int result = codeVerifier.redeemCode(input.getText().toString());

                if (result == CodeVerifier.CODE_FAILED_NO_SUCH_CODE) {
                    String message = getResources().getString(R.string.redemption_redeem_dialog_error_no_code);
                    DialogUtil.createErrorDialog(message, RedemptionActivity.this, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            showRedeemDialog();
                        }
                    });
                } else if (result == CodeVerifier.CODE_FAILED_REDEEMED) {
                    String message = getResources().getString(R.string.redemption_redeem_dialog_error_redeemed);
                    DialogUtil.createErrorDialog(message, RedemptionActivity.this);
                } else {
                    refreshCylinder();
                }
            }
        });

        builder.setNegativeButton(R.string.redemption_redeem_dialog_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showRedeemedDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.redemption_sl_redeemed_dialog_title);

        // Set the custom view
        @SuppressLint("InflateParams")
        View prizeDialogView = this.getLayoutInflater().inflate(R.layout.layout_dialog_sl_redeem, null);

        TextView textViewDsa = prizeDialogView.findViewById(R.id.textview_redemption_dialog_dsa);
        TextView textViewRedeemed = prizeDialogView.findViewById(R.id.textview_redemption_dialog_redeemed);

        // Set the DSA text
        String dsaText = getResources().getString(R.string.redemption_sl_redeemed_dialog_dsa);
        dsaText += this.codeVerifier.DSACodeRedeemed() ? "Yes" : "No";
        textViewDsa.setText(dsaText);

        // Set the redeemed text
        String redeemedText = getResources().getString(R.string.redemption_sl_redeemed_dialog_redeemed);
        redeemedText += this.codeVerifier.getRedeemed() ? "Yes" : "No";
        textViewRedeemed.setText(redeemedText);

        builder.setView(prizeDialogView);

        // Set the button functions
        builder.setPositiveButton(R.string.redemption_sl_redeemed_dialog_redeem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showConfirmRedeemedDialog();
            }
        });

        builder.setNegativeButton(R.string.redemption_sl_redeemed_dialog_close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showConfirmRedeemedDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.redemption_sl_confirm_dialog_title)
                .setMessage(R.string.redemption_sl_confirm_dialog_desc);

        // Set the custom view to a numerical input
        final EditText input = DialogUtil.createCodeInput(this, 4);
        builder.setView(input);

        // Set the button functions
        builder.setPositiveButton(R.string.redemption_sl_confirm_dialog_confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (input.getText().toString().equals(getResources().getString(R.string.redemption_sl_code))) {
                    codeVerifier.setRedeemed();
                }
            }
        });

        builder.setNegativeButton(R.string.redemption_sl_confirm_dialog_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
