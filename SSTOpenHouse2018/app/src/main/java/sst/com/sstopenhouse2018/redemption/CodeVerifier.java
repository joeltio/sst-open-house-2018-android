package sst.com.sstopenhouse2018.redemption;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import java.util.HashMap;

public class CodeVerifier {
    private HashMap<String, String> redemptionCodes;
    private SharedPreferences codePreferences;
    private SharedPreferences sharedPreferences;

    @SuppressWarnings("WeakerAccess")
    public static final int CODE_OK = 0;
    public static final int CODE_FAILED_NO_SUCH_CODE = 1;
    public static final int CODE_FAILED_REDEEMED = 2;

    private static final String PREFERENCE_DSA_CODE_REDEEMED = "dsa_code_redeemed";
    private static final String PREFERENCES_REDEEMED = "redeemed_prizes";

    private FirebaseDatabaseAdapter firebaseDatabaseAdapter;

    public CodeVerifier(SharedPreferences codePreferences, SharedPreferences sharedPreferences) {
        this.redemptionCodes = null;
        this.codePreferences = codePreferences;
        this.sharedPreferences = sharedPreferences;
        this.firebaseDatabaseAdapter = new FirebaseDatabaseAdapter();
        firebaseDatabaseAdapter.open();
    }

    private HashMap<String, String> getRedemptionCodes() {
        return this.firebaseDatabaseAdapter.getRedemptionCodes();
    }

    private void incrementCode(String code) {
        this.firebaseDatabaseAdapter.increment(code);
    }

    private boolean isCodeDSA(String code) {
        return code.equals("JQWN");
    }

    private boolean verifyCode(String code) {
        if (this.redemptionCodes == null) {
            this.redemptionCodes = getRedemptionCodes();
        }
        return this.redemptionCodes.containsKey(code);
    }

    @SuppressLint("ApplySharedPref")
    public int redeemCode(String code) {
        if (verifyCode(code)) {
            // Check if the code has already been redeemed
            if (!this.codePreferences.getBoolean(code, false)) {
                incrementCode(code);

                SharedPreferences.Editor editor = this.codePreferences.edit();
                editor.putBoolean(code, true);
                editor.commit();

                // Check if the code is the DSA booth's code
                if (isCodeDSA(code)) {
                    this.sharedPreferences.edit().putBoolean(PREFERENCE_DSA_CODE_REDEEMED, true).commit();
                }

                return CODE_OK;
            } else {
                return CODE_FAILED_REDEEMED;
            }
        } else {
            return CODE_FAILED_NO_SUCH_CODE;
        }
    }

    public boolean DSACodeRedeemed() {
        return this.sharedPreferences.getBoolean(PREFERENCE_DSA_CODE_REDEEMED, false);
    }

    public int getNumCodesRedeemed() {
        return this.codePreferences.getAll().size();
    }

    public void setRedeemed() {
        SharedPreferences.Editor edit = this.sharedPreferences.edit();
        edit.putBoolean(PREFERENCES_REDEEMED, true);
        edit.apply();
    }

    public boolean getRedeemed() {
        return sharedPreferences.getBoolean(PREFERENCES_REDEEMED, false);
    }
}
