package sst.com.sstopenhouse2018.redemption;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import java.util.HashMap;

public class FirebaseDatabaseAdapter {
    private FirebaseDatabase database;
    private DatabaseReference root;

    void open() {
        this.database = FirebaseDatabase.getInstance();
        this.root = this.database.getReference();
    }

    void increment(String code) {
        String name = getRedemptionCodes().get(code);
        this.root.child(name).child("count").runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData currentData) {
                Integer i = currentData.getValue(Integer.class);

                if (i != null) {
                    i++;
                    currentData.setValue(i);
                }
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                if (databaseError != null) {
                    databaseError.toException().printStackTrace();
                }
            }
        });
    }

    HashMap<String, String> getRedemptionCodes() {
        // TODO: Get this data from firebase
        HashMap<String, String> redemptionCodes = new HashMap<>();
        redemptionCodes.put("CMRD", "PVP Talk");
        redemptionCodes.put("JNRF", "Student Panel");
        redemptionCodes.put("QPSH", "Student Life Gallery");
        redemptionCodes.put("JQWN", "DSA");
        redemptionCodes.put("SFHY", "PforSST");
        redemptionCodes.put("QUDY", "Astronomy");
        redemptionCodes.put("UWQW", "Athletics");
        redemptionCodes.put("MXYR", "Basketball");
        redemptionCodes.put("KYUJ", "Fencing");
        redemptionCodes.put("AJHL", "Media Club");
        redemptionCodes.put("XWBG", "Showchoir");
        redemptionCodes.put("SLGL", "Scouts");
        redemptionCodes.put("VCGP", "Robotics");
        redemptionCodes.put("FSGN", "SYFC");
        redemptionCodes.put("WTGJ", "Chemistry Showcase");
        redemptionCodes.put("GJDE", "Physics Showcase");
        redemptionCodes.put("DXDK", "Biology Showcase");
        redemptionCodes.put("YLYC", "Biotechnology Showcase");
        redemptionCodes.put("VXBW", "Electronics Showcase");
        redemptionCodes.put("UHWA", "Science TDP");
        redemptionCodes.put("CNQR", "ChangeMakers");
        redemptionCodes.put("RAKG", "ACE");
        redemptionCodes.put("EHBB", "English");
        redemptionCodes.put("SXQE", "IH");
        redemptionCodes.put("MVRD", "Mathematics");
        redemptionCodes.put("XNAK", "Chinese");
        redemptionCodes.put("STUQ", "Malay");
        redemptionCodes.put("UDSE", "Tamil");
        redemptionCodes.put("ZLEZ", "SSTED Talks");
        redemptionCodes.put("JBGK", "Computing");
        redemptionCodes.put("KJHB", "Robotics CCA Showcase");
        redemptionCodes.put("CRWN", "Exhibition Centre Showcase");
        redemptionCodes.put("GPYF", "Everyday Innovations");
        redemptionCodes.put("PSJQ", "Student Leaders");
        redemptionCodes.put("MDWZ", "Sports and Wellness");
        redemptionCodes.put("PCMR", "Design Studies");
        return redemptionCodes;
    }
}
