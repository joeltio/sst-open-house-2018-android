package sst.com.sstopenhouse2018.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;

import sst.com.sstopenhouse2018.R;

public class DialogUtil {
    public static void createErrorDialog(String message, Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.all_dialog_error_title).setMessage(message);

        builder.setNeutralButton(R.string.all_dialog_error_close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.create().show();
    }
    public static void createErrorDialog(String message, Activity activity,
                                         DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.all_dialog_error_title).setMessage(message);

        builder.setNeutralButton(R.string.all_dialog_error_close, onClickListener);

        builder.create().show();
    }

    public static EditText createCodeInput(Context context, int charLength) {
        EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        input.setFilters(new InputFilter[] { new InputFilter.LengthFilter(charLength) });
        input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        input.setAllCaps(true);

        return input;
    }
}
