package sst.com.sstopenhouse2018.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import java.io.File;

public class DynamicImageView extends AppCompatImageView {
    private int imageWidth;
    private int imageHeight;

    public DynamicImageView(Context context) {
        super(context);
    }

    public DynamicImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DynamicImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setImageResource(int resId) {
        BitmapFactory.Options dimensions = new BitmapFactory.Options();
        dimensions.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), resId, dimensions);
        this.imageWidth =  dimensions.outWidth;
        this.imageHeight = dimensions.outHeight;
        super.setImageResource(resId);
    }

    @Override
    public void setImageDrawable(@Nullable Drawable drawable) {
        this.imageHeight = drawable.getIntrinsicHeight();
        this.imageWidth = drawable.getIntrinsicWidth();
        super.setImageDrawable(drawable);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        this.imageWidth = bm.getWidth();
        this.imageHeight = bm.getHeight();
        super.setImageBitmap(bm);
    }

    @Override
    public void setImageURI(@Nullable Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(new File(uri.getPath()).getAbsolutePath(), options);
        this.imageHeight = options.outHeight;
        this.imageWidth = options.outWidth;
        super.setImageURI(uri);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = getDefaultSize(this.imageWidth, widthMeasureSpec);
        int height = getDefaultSize(this.imageHeight, heightMeasureSpec);
        if (this.imageWidth > 0 && this.imageHeight > 0) {
            int desiredWidth = MeasureSpec.getSize(widthMeasureSpec);
            height = desiredWidth * this.imageHeight / this.imageWidth;
        }

        setMeasuredDimension(width, height);
    }
}
