package sst.com.sstopenhouse2018.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * A Dynamic Video View that maintains the aspect ratio of the video
 * The video view will match the height to the width.
 */
public class DynamicVideoView extends VideoView {
    private int videoWidth;
    private int videoHeight;

    public DynamicVideoView(Context context) {
        super(context);
    }

    public DynamicVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DynamicVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setVideoResource(int videoResource, String packageName, Context application) {
        String path = "android.resource://" + packageName + "/raw/" + videoResource;
        Uri uri = Uri.parse(path);

        // Get the dimensions and store them
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(application, uri);
        Bitmap bmp = retriever.getFrameAtTime();

        this.videoWidth = bmp.getWidth();
        this.videoHeight = bmp.getHeight();

        // Set the video
        this.setVideoURI(Uri.parse(path));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = getDefaultSize(this.videoWidth, widthMeasureSpec);
        int height = getDefaultSize(this.videoHeight, heightMeasureSpec);
        if (this.videoWidth > 0 && this.videoHeight > 0) {
            int desiredWidth = MeasureSpec.getSize(widthMeasureSpec);
            height = desiredWidth * this.videoHeight / this.videoWidth;
        }

        setMeasuredDimension(width, height);
    }
}
