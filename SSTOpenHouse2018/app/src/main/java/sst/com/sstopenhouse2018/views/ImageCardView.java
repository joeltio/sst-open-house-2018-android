package sst.com.sstopenhouse2018.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.TextView;

import sst.com.sstopenhouse2018.R;

public class ImageCardView extends CardView {
    private DynamicImageView dynamicImageView;
    private TextView title;
    private TextView description;

    public ImageCardView(@NonNull Context context) {
        super(context);
        init(context, null);
    }

    public ImageCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ImageCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        inflate(getContext(), R.layout.layout_card_image, this);

        if (attrs != null) {
            this.dynamicImageView = findViewById(R.id.imageview);
            this.title = findViewById(R.id.textview_title);
            this.description = findViewById(R.id.textview_desc);

            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs, R.styleable.ImageCardView, 0, 0);
            try {
                this.title.setText(a.getString(R.styleable.ImageCardView_title));
                this.description.setText(a.getString(R.styleable.ImageCardView_description));

                int imageResource = a.getResourceId(R.styleable.ImageCardView_src, 0);
                if (imageResource != 0) {
                    this.dynamicImageView.setImageResource(imageResource);
                }
                this.dynamicImageView.setContentDescription(a.getString(
                        R.styleable.ImageCardView_imageContentdescription));
            } finally {
                a.recycle();
            }
        }
    }
}
